import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"

export const WeatherImage = ({weatherMain}) => {
    
    switch(weatherMain) {
        case "Thunderstorm": 
            return (
                <FontAwesomeIcon icon="fa-solid fa-cloud-bolt" />
        )
        case "Drizzle": 
            return (
                <FontAwesomeIcon icon="fa-solid fa-cloud-rain" />
        )
        case "Rain": 
            return (
                <FontAwesomeIcon icon="fa-solid fa-cloud-showers-heavy" />
        )
        case "Snow": 
            return (
                <FontAwesomeIcon icon="fa-solid fa-snowflake" />
        )
        case "Clouds":
            return (
                <FontAwesomeIcon icon="fa-solid fa-cloud" />
            )
        case "Clear": 
            return (
                <FontAwesomeIcon icon="fa-regular fa-sun" />
        )
        case "Haze":
            return(
                <FontAwesomeIcon icon="fa-solid fa-cloud" />
            )
        default:
            return (
                <FontAwesomeIcon icon="fa-solid fa-mountain-sun" />
            )
        
    }
}