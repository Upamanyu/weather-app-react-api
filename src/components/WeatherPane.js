import React from "react";
import {WeatherImage} from "./WeatherImage";

function WeatherPane({weather,loc}) {
    const months= ["Jan","Feb","Mar","Apr","May","Jun","July","Aug","Sep","Oct","Nov","Dec"];
    const temp = Math.round(weather.temp);
    // create weather condition object for conditional rendering of icons
    // pass object to WeatherImage
    const day = new Date().getDate();
    const monthIndex = new Date().getMonth();  
    return (
        <>
        {/* write weather pane html here */}
        
        <div className="weatherIcon">
            {/* conditionally render based on conditions */}
            <WeatherImage weatherMain={weather.main} />
        </div>
            <div className="weatherInfo">
                
            <div className="temperature"><span>{temp}&deg; C</span></div>
                <div className="description">    
            <div className="weatherCondition">{weather.description}</div>    
            <div className="place">{loc[0]} , {loc[1]}</div>
            </div>
           </div>
            <div className="date">{day} {months[monthIndex]}</div>

        </>
    )
}

export default WeatherPane;