import logo from './logo.svg';
import axios from 'axios';
import './App.css';
import WeatherPane from './components/WeatherPane'
import { useEffect, useState } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

let countryState = 'West Bengal';
function App() {
  
const [location,setLocation] = useState('Kolkata');
const [weatherData,setWeatherData] = useState({}); 
 
  useEffect(()=>{
      // API CALL
      const geoUrl = `http://api.openweathermap.org/geo/1.0/direct?q=${location}&appid=${process.env.REACT_APP_MYAPIKEY}`;
      
      axios.get(geoUrl)
      .then( geoResult=>{
        // use the geolocation api data to get latitude and longitude and call weather api
        countryState = geoResult.data[0].state? geoResult.data[0].state: geoResult.data[0].country;
        const latitude = geoResult.data[0].lat
        const longitude = geoResult.data[0].lon
        const weatherURL = `https://api.openweathermap.org/data/2.5/weather?lat=${latitude}&lon=${longitude}&units=metric&appid=${process.env.REACT_APP_MYAPIKEY}`;
        return axios.get(weatherURL);
      })
      .then( weatherResult=>{

        const weatherDetails = {...weatherResult.data.main,
          ...weatherResult.data.weather[0],
          ...weatherResult.data.wind,
          ...weatherResult.data.clouds
        }
          
          setWeatherData(weatherDetails);
      })  
      .catch(()=> {
          setLocation('Kolkata');
          countryState='West Bengal';
          // add alert box here to tell user to enter proper city
          alert("error in receiving data from weather or location api! please enter proper city name or try after some time")
        });


  },[location]);

  // on submit function

  function onSubmit(e) {
      let newInput = e.currentTarget.previousSibling.value;
      newInput = newInput.toLowerCase();
      setLocation(newInput);

  }

  return (
    <>
    <article className="weather-grid">
    {/* send weather and location data */}
    <WeatherPane weather={{...weatherData}} loc={[location,countryState]}  />
    <div className="input-form">
    <input type="text" name="city" placeholder="Enter city" required />
    <button type="submit" onClick={onSubmit}>Get weather <FontAwesomeIcon icon="fa-solid fa-magnifying-glass" /></button>
    </div>
    </article>
    <footer>Created by Upamanyu Mukharji &#8482;</footer>
    </>
  );
}

export default App;
